package com.dapeng.lmssim.client;

import lombok.Data;

@Data
public class IMErrorResponse {
    private String code;
    private String data;
    private String msg;
}
