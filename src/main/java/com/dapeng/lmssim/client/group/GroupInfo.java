
package com.dapeng.lmssim.client.group;

import lombok.Data;

/**
 * 使用idea插件:Json2Pojo with Lombok,实现根据json字符串生成lombok风格的pojo对象
 */
@Data
public class GroupInfo {

    private String applyJoinOption;
    private long createTime;
    private long errorCode;
    private String errorInfo;
    private String faceUrl;
    private String groupId;
    private String introduction;
    private long lastInfoTime;
    private long lastMsgTime;
    private long maxMemberNum;
    private long memberNum;
    private String name;
    private long nextMsgSeq;
    private String notification;
    private String ownerAccount;
    private String shutUpAllMember;
    private String type;

}
