package com.dapeng.lmssim.client;

public class IMServiceException extends RuntimeException {
    public IMServiceException(String message) {
        super(message);
    }
}
