package com.dapeng.lmssim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class LmssImApplication {

    public static void main(String[] args) {
        SpringApplication.run(LmssImApplication.class, args);
    }

}
