package com.dapeng.lmssim.web;

import com.dapeng.lmssim.client.group.GroupClient;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author songkaizong
 */
@RestController
@AllArgsConstructor
public class ClassController {
    private GroupClient groupClient;

    @GetMapping("/class/{id}")
    public ResponseEntity delGroup(@PathVariable String id) {
        //classRepository.delete(id)
//        groupClient.deleteGroup(id);
        return ResponseEntity.ok(id);
    }
}
