package com.dapeng.lmssim;

import com.alibaba.fastjson.JSON;
import com.dapeng.lmssim.client.group.GroupClient;
import com.dapeng.lmssim.client.group.GroupInfo;
import com.netflix.hystrix.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class LmssImApplicationTests {
    @Autowired
    private GroupClient groupClient;


    @Test
    void get() {
        GroupInfo groupDetail = groupClient.getGroup("123");
        System.out.println(JSON.toJSONString(groupDetail));
    }

    @Test
    void del() {
        groupClient.deleteGroup("123");
    }

    @Test
    void updateOwner() {
        groupClient.updateOwner("123", "123");
    }

    @Test
    void logout() {
        groupClient.logout();
    }

}
