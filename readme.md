#### 错误判断：

调用方应当根据HTTP Response Status判断请求是否成功，请求失败有2种情况，
一是网关，Tomcat服务器返回，此时请求还没有到达接口（如权限，请求url、method错误等），
另外就是接口处理返回，通常是业务异常。

#### HTTP STATUS

- 2XX，请求成功
- 3XX，请求重定向
- 4XX，是客户端请求错误，或者业务异常
- 5XX，是服务器异常

#### 错误处理

接口调用方的错误处理逻辑是：首先判断HTTP返回码，
- 如果为5XX，按照未知异常处理（一般情况是服务器挂了或网关路由问题）。
- 如果为4XX，诸如请求参数非法（400），权限（401,403），请求方式不允许（405），在开发及测试时就已经排除，正常情况下应当都是业务异常，应当将此类response body解析为异常对象，如果JSON解析失败，按照未知异常处理。
- 如果为2XX，接口请求成功，如果是GET请求，解析response body。其他请求方式没有response body。

#### feign及hystrix异常处理过程

当声明接口如下（这里返回值不需要设置为ResponseEntity<> ,除非你希望获取响应状态，头信息等自己来处理）
```json
    @GetMapping("/groups/{id}")
    GroupInfo getGroup(@PathVariable String id);
```
当响应状态非2XX时，会调用@FeignClient 配置的configuration的ErrorDecoder的decode方法，返回一个Exception，这个方法只是用来自定义返回的异常信息，默认情况下是FeignException，
如果配置了hystrix的fallback，最终会携带异常信息，调用对应的fallback方法（此时上一步的配置可省略，只是为了这个解析提供方便），fallback 方法中可以选择返回默认值，记录异常信息，或继续向上层抛异常。
